# Other information here #

## check for updates then update ##

* npm outdated
* npm update

## Articles ##

* [finally in promise](http://2ality.com/2017/07/promise-prototype-finally.html)
* [Writing neat asynchronous Node JS code with Promises](https://medium.com/dev-bits/writing-neat-asynchronous-node-js-code-with-promises-32ed3a4fd098)

## packages/libraries ##

* [axios - promise based html client](https://www.npmjs.com/package/axios)
* [commander - command-line handling](https://www.npmjs.com/package/commander)


## Videos ##

* [Publishing to npmjs](https://www.youtube.com/watch?v=BkotrAFtBM0)
* [Semantic Versioning](https://www.youtube.com/watch?v=kK4Meix58R4)
* [Updating local packages](https://www.youtube.com/watch?v=HRudtPGcOt4&list=PLQso55XhxkgBMeiYmFEHzz1axDUBjTLC6&index=5)