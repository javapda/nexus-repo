# examples #

## pom-model ##

```
node 
```

# get latest snapshot version of an artifact #

```
  myconfig = {command: "latest-snapshot-meta", 
               g: "my-group-id",
               a: "my-artifact-id",
               repoUrl: "http://my-nexus-repo/nexus"
             }
  nexusRepo(myconfig) 
  .then((result)=>{
      console.log(`Latest snapshot version: ${result.version}`)
  }, (err)=>{})

```
