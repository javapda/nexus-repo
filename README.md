# nexus-repo #

* promise-based interaction with a nexus repository
* in the early days there will be frequent updates
* tested on [Nexus Repository Manager OSS](https://help.sonatype.com/repomanager2) 2.14.9-01

## Commands ##

* list-repos
* pom-model
* retrieve-artifact
* search-maven-central

# Installation #

Using npm:

```
 $ npm install nexus-repo --save
```

# Usage #

In Node.js:

```
var {nexusRepo} = require("nexus-repo")

nexusRepo({command:'search-maven-central', searchMavenCentral:'log4j'})
```
## NOTE ##

* you can place a file in your home directory called *.nexusRepo.json* where you can place common information and/or credentials. For example:
```
{
    "username" : "myusername",
    "password" : "mypassword",
    "repoUrl" : "http://mylocal.domain.com/nexus/",
    "verbose" : false
}
```

# docs #

* [Nexus Core API (Restlet 1.x Plugin) REST API](https://repository.sonatype.org/nexus-restlet1x-plugin/default/docs/index.html)
* [better documentation on git site](https://bitbucket.org/javapda/nexus-repo)
* [docs/examples.md](docs/examples.md)
* [docs/other.md](docs/other.md)

# How to publish #

* // after git committing
* _npm version patch_
* _npm publish --access public_