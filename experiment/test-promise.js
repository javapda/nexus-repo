const {promiseFunction} = require('./promise')

console.log(promiseFunction)
//throw 'bad'

promiseFunction('good')
.then(function(result) {
    console.log('success, result='+result)
}, function(err) {
    console.log('error, err='+err)
})
.catch(function(err){
    console.log("SOMETHING WENT WRONG, err="+err)
})