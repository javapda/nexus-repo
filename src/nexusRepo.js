const axios = require('axios')
const fs = require('fs')
const _ = require('lodash')
const getHomeNexusConfigJson=() => {
    let config = {}
    try {
        fs.accessSync(`${process.env.HOME}/.nexusRepo.json`, fs.constants.R_OK | fs.constants.W_OK);
        config = require(`${process.env.HOME}/.nexusRepo.json`)
      } catch (err) {
        //console.error('no access!');
      }
      return config
    
}
const searchMavenCentral=(config)=>{
    if (!config.searchMavenCentral) {
        throw "missing searchMavenCentral"
    }
    if(config.verbose) {console.log(`searchMavenCentral time with ${config.searchMavenCentral}`);}
    const rows = (config.rows) || 10
    const start = (config.start) || 0
    const url=`https://search.maven.org/solrsearch/select?q=${config.searchMavenCentral}&start=${start}&rows=${rows}`
    return axios.get(`${url}`,{headers: {'Accept':'application/json'}})
}

const listRepos=(config) => {

    if(config.verbose) {console.log(`listing repos`);}
    const url = `${config.repoUrl}/service/local/all_repositories`
    console.log('url='+url)
    const params={headers: {'Accept':(config.mimeType)?config.mimeType:'application/json'}}
    return axios.get(url,params)
}
const latestReleaseMeta=(config) => {
    if(config.verbose) {
        console.log(`${config.command}`);
        console.log(`config is ${JSON.stringify(config)}`);
    }
    const props={headers: {
        'Accept':'application/json'
    }}
    const {g,a} = config
    const url=`${config.repoUrl}/service/local/artifact/maven/resolve?g=${g}&a=${a}&v=LATEST&r=releases&p=pom`;
    if(config.verbose) {
        console.log('url===>'+url)
    }
    
    return new Promise(function(resolve,reject){
        // do async job
        axios.get(url,props)
        .then((result)=>{
            resolve(result.data.data)
        })
        .catch((err)=>{
            reject(err)
        })
    });    

}
const latestSnapshotMeta=(config) => {
    if(config.verbose) {
        console.log(`${config.command}`);
        console.log(`config is ${JSON.stringify(config)}`);
    }
    const props={headers: {
        'Accept':'application/json'
    }}
    const {g,a} = config
    const url=`${config.repoUrl}/service/local/artifact/maven/resolve?g=${g}&a=${a}&v=LATEST&r=snapshots&p=pom`;
    if(config.verbose) {
        console.log(`url: ${url}`)
    }
    return new Promise(function(resolve,reject){
        // do async job
        axios.get(url,props)
        .then((result)=>{
            resolve(result.data.data)
        })
        .catch((err)=>{
            reject(err)
        })
    });    


}

const retrieveArtifact=(config) => {
    if(config.verbose) {
        console.log(`${config.command}`);
        console.log(`config is ${JSON.stringify(config)}`);
    }
    // this one requires */* for Accept Header
    const props={headers: {
        'Accept':'*/*'
    }}
    if (config.username) {
       props.auth={username:config.username, password:config.password}
    }
    if(config.verbose) {
        console.log(`props===>${JSON.stringify(props)}`);
    }
    const {g,a,r,p} = config
    const url=`${config.repoUrl}/service/local/artifact/maven/content?g=com.gs&a=gs-util&v=LATEST&r=snapshots&p=pom`;
    return axios.get(url,props)
}

const pomModel = (config) => {
    if(config.verbose) {
        console.log("pom-model time");
        console.log(`listing repos`)
    }
    const {g,a,v,r} = config;
    const url=`${config.repoUrl}/service/local/artifact/maven?g=${g}&a=${a}&v=${v}&r=${r}`;
    if(config.verbose){console.log('g='+g+', a='+a+', v='+v+', r='+r+', url='+url);}
    return axios.get(url,
    {headers: {'Accept':'application/json'}})
}
const commands = {
    "retrieve-artifact" : retrieveArtifact,
    "latest-release-meta" : latestReleaseMeta,
    "latest-snapshot-meta" : latestSnapshotMeta,
    "list-repos": listRepos,
    "pom-model": pomModel,
    "search-maven-central": searchMavenCentral
}

exports.showCommands = function() {
    console.log('available commands')
    for (var i in commands) {
        console.log(i)
    }
}
exports.nexusRepo = function(inconfig) {
    const config = _.merge(getHomeNexusConfigJson(), inconfig)
    if (config.verbose) {
        console.log('----------------------------')
        console.log(JSON.stringify(config))
        console.log('----------------------------')
    }
    const command = config.command
    if (command) {
        if(config.verbose){console.log(`command is ${command}`);}
        if (commands[command]) {
            return commands[command](config)
        } else {
            throw `unknown command "${command}"`
        }
    } else {
        throw `missing command (not provided)`
    }
}

