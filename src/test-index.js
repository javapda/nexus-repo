const {nexusRepo} = require('../index')
const fs = require('fs')
var _ = require('lodash');
let config = {}

const testLatestSnapshotMeta = () => {
    console.log('testLatestSnapshotMeta()')
    let myconfig = _.clone(config)
    myconfig.command= "latest-snapshot-meta"
  nexusRepo(myconfig) 
  .then((result)=>{
      console.log(`Result: ${JSON.stringify(result)}`)
      console.log(`Latest snapshot version: ${result.version}`)
  }, (err)=>{})
}

const testLatestReleaseMeta = () => {
    console.log('testLatestReleaseMeta()')
    let myconfig = _.clone(config)
    myconfig.command= "latest-release-meta"
  nexusRepo(myconfig) 
  .then((result)=>{
      console.log(`Result: ${JSON.stringify(result)}`)
      console.log(`Version: ${result.version}`)
  }, (err)=>{
      console.log('problem, err='+err)
  })
}
  const testSearchMavenCentral = () => {
    let myconfig = _.clone(config)
    myconfig.command= "search-maven-central"
  nexusRepo(myconfig) 
  .then((result)=>{
      //console.log(result.data)
      //console.log('back from searching maven central')
      console.log(`No. docs: ${result.data.response.docs.length}`)
  }, (err)=>{})
}  

const testListRepos = () => {
    console.log('listRepos')
    let myconfig = _.clone(config)
    myconfig.command= "list-repos"
  nexusRepo(myconfig) 
  .then((result)=>{
      console.log("HOOOWO")
      if (myconfig.verbose) {
        console.log(`back from ${myconfig.command}`)
      }
      console.log(result.data)
  }, (err)=>{
      console.log("PROBLEM, err="+err)
  })
}

const testRetrieveArtifact = () => {
    let myconfig = _.clone(config)
    myconfig.command= "retrieve-artifact"
    nexusRepo(myconfig)
    .then(function(result){
        console.log(result.data)
        //console.log("VERSION:"+(result.data.version||result.data.parent.version))
        console.log(`back from ${myconfig.command}`)
    },function(err){
        console.log("err found "+err)
    })
    .catch(function(err){
        console.log('caught problem '+err)
    })

}

const testPomModel = () => {
    let myconfig = _.clone(config)
    myconfig.command= "pom-model"
    nexusRepo(myconfig)
    .then(function(result){
        //console.log(result)
        console.log("VERSION:"+(result.data.version||result.data.parent.version))
    },function(err){
        console.log("err found "+err)
    })
    .catch(function(err){
        console.log('caught problem '+err)
    })
}

// testPomModel()
// testListRepos()
//testSearchMavenCentral()
//testRetrieveArtifact()
//testLatestReleaseMeta()
testLatestSnapshotMeta()