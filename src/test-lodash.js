const _ = require('lodash')

const obj1 = {"a": "fred", "b": "hector"}
const obj2 = {"a": "barney", "b": "wilma"}

console.log(_.merge(obj1,obj2))