const {nexusRepo,showCommands} = require('./nexusRepo');

nexusRepo({command:'list-repos'})
.then((resy)=> {
    var cache = [];
    var str=JSON.stringify(resy, function(key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                // Duplicate reference found
                try {
                    // If this value does not reference a parent it can be deduped
                    return JSON.parse(JSON.stringify(value));
                } catch (error) {
                    // discard key if value cannot be deduped
                    return;
                }
            }
            // Store value in our collection
            cache.push(value);
        }
        return value;
    });
    cache = null;    
    console.log('str='+str)
}, (err)=>{
    console.log('err='+err)
})
.catch((err)=>{
    console.log("problem...err="+err)
})

