var colors = require('colors');
var program = require('commander')
const fs = require('fs');
//var config = require('~/.nexusRepo.json')
var {nexusRepo} = require('./src/nexusRepo')
var {version} = require('./package.json')
console.log('command-line interface to nexusRepo')
console.log(`process.env.HOME: ${process.env.HOME}`)
console.log('version:'+version)
let config = {}
try {
    fs.accessSync(`${process.env.HOME}/.nexusRepo.json`, fs.constants.R_OK | fs.constants.W_OK);
    console.log('can read/write');
    config = require(`${process.env.HOME}/.nexusRepo.json`)
  } catch (err) {
    //console.error('no access!');
  }
const pomModelParameters = `'{"g":"com.gs","a":"gs-util","v":"LATEST","r":"releases"}'`

program
  .version(version)
  .option('--username <username>', 'if authentication is needed, username of privileged nexus account')
  .option('--password <password>', 'if authentication is needed, password of privileged nexus account')
  .option('--repo-url <url-to-nexus-repo>', 'comma-separated list of slack channels (#venn, #treatment)')
  .option('--command <command>', 'command of interest')
  .option('--parameters <json-string-command-parameters', 'parameters to satisfy command', `{"wilma":"jones"}`)
  .option('--search-maven-central <search-string>', 'searches the maven central repository')
  .option('--verbose','lists resources')
  

  program.on('--help', function() {
    console.log('')
    console.log(`Note: `)
    console.log(` default entries can be placed in ${process.env.HOME}/.nexusRepo.json using camelBacked keys`.italic.red) 
    console.log('Examples:')
    console.log(`  $ ${__filename} `)
    console.log(`  $ ${__filename} --help`)
    console.log(`  $ ${__filename} --repo-url https://my.local.nexusrepo/`)
    console.log(`  $ ${__filename} --repo-url https://my.local.nexusrepo/ --command pom-model --parameters ${pomModelParameters}`)
    console.log(`  $ ${__filename} --command pom-model --parameters ${pomModelParameters}`)
  })


program.parse(process.argv)
if (program.username) config.username = program.username
if (program.password) config.password = program.password
if (program.repoUrl) config.repoUrl = program.repoUrl
if (program.command) config.command = program.command
if (program.parameters) config.parameters = JSON.parse(program.parameters)
if (program.verbose) config.verbose = true
if (program.searchMavenCentral) {
    config.command = 'search-maven-central'
    config.searchMavenCentral = program.searchMavenCentral
}
if (config.verbose) {
  console.log('config:'+JSON.stringify(config))  
}
nexusRepo(config)
.then(function(result){
  console.log(result)
},function(err){
  console.log("PROBLEM, err="+err)
})
